﻿using HSCalc;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(DoneEntry), typeof(HSCalc.Droid.DoneEntryRenderer))]
namespace HSCalc.Droid {
	public class DoneEntryRenderer : EntryRenderer {
		public DoneEntryRenderer() {
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e) {
			base.OnElementChanged(e);
		}
	}
}