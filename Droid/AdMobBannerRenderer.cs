﻿using HSCalc;
using HSCalc.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(AdMobBanner), typeof(HSCalc.Droid.Renderers.AdMobBannerRenderer))]
namespace HSCalc.Droid.Renderers
{
	public class AdMobBannerRenderer : ViewRenderer<AdMobBanner, Android.Gms.Ads.AdView>
	{
		protected override void OnElementChanged(ElementChangedEventArgs<AdMobBanner> e)
		{
			const string adUnitID = "ca-app-pub-1154104535471881/4661932454";

			base.OnElementChanged(e);

			if (Control == null)
			{
				var adMobBanner = new Android.Gms.Ads.AdView(Forms.Context);
				adMobBanner.AdSize = Android.Gms.Ads.AdSize.Banner;
				adMobBanner.AdUnitId = adUnitID;

				var requestbuilder = new Android.Gms.Ads.AdRequest.Builder();
				requestbuilder.SetGender(Android.Gms.Ads.AdRequest.GenderMale);
				adMobBanner.LoadAd(requestbuilder.Build());

				SetNativeControl(adMobBanner);
			}
		}
	}
}