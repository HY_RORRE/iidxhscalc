﻿using Xamarin.Forms;
[assembly: Dependency(typeof(AssemblyService))]
public class AssemblyService : IAssemblyService {
	//アプリ名称を取得する
	public string GetPackageName() {
		var context = Android.App.Application.Context;    //Android.App.Application.Context;
		var name = context.PackageManager.GetPackageInfo(context.PackageName, 0).PackageName;
		return name;
	}

	//アプリバージョン文字列を取得する
	public string GetVersionName() {
		var context = Android.App.Application.Context;    //Android.App.Application.Context;
		var name = context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionName;
		return name;
	}

}