﻿using System;

using Xamarin.Forms;

using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace HSCalc {
	public class SafeAreaPage : ContentPage {
		public SafeAreaPage() {
			On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
		}
	}
}

