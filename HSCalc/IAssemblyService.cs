﻿public interface IAssemblyService {
	string GetPackageName();
	string GetVersionName();
}