﻿using System;
using System.Net;
using System.Net.Http;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using CsvHelper;
using CsvHelper.Configuration;

using PCLStorage;

using Newtonsoft.Json;

using Xamarin.Forms;

namespace HSCalc {
	public class App : Application {

		#region variable_declaration
		static readonly int PADDING_TOP = 0; //(Device.RuntimePlatform == Device.iOS ? 20 : 0);
		const int MAX_HS_CHANGE_TIME = 16;
		const int MAX_HSBPM = 750;

		bool is_music_csv_loaded = false;

		SharedEntry entryDisplayTime = new SharedEntry(MAX_ENTRY);
		SharedEntry entryBpmStart = new SharedEntry(MAX_ENTRY);
		SharedEntry entryBpmAdjust = new SharedEntry(MAX_ENTRY);
		Switch switchAdjustBpmStart;

		HsChange[] arrayHsChange;
		ListView listViewHsChange;
		ViewCellHsChange[] arrayViewCellHsChange;

		NhsChange[] arrayNhsChange;
		ListView listViewNhsChange;
		ViewCellNhsChange[] arrayViewCellNhsChange;

		SafeAreaPage pageMusic;

		ObservableCollection<ViewCellMusic> listViewCellMusic = new ObservableCollection<ViewCellMusic>();

		static AdMobBanner[] adbanners = new AdMobBanner[MAX_PAGE];

		int tapcount = 0;
		ViewCellMusic last_tapped_cell;

		const int PAGE_DISPLAY_TIME_CALC = 0;
		const int PAGE_FHS_CALC = 1;
		const int PAGE_NHS_CALC = 2;
		const int PAGE_FHS_TO_NHS = 3;

		const int MAX_ENTRY = 4;

		const int PAGE_MUSIC_LIST = 4;
		const int PAGE_MUSIC_DETAIL = 5;

		const int MAX_PAGE = 6;

		const int MAX_NHS_N = 11;
		#endregion

		#region classes
		class SharedEntry {
			public DoneEntry[] entry;
			public Stepper[] stepper;
			int size;
			private SharedEntry() { }
			public SharedEntry(int size) {
				this.size = size;
				entry = new DoneEntry[size];
				stepper = new Stepper[size];
				for(int i = 0; i < size; ++i) {
					entry[i] = new DoneEntry();
					stepper[i] = new Stepper {
						Minimum = 0,
						Maximum = 20000,
						Increment = 1,
						Value = 10000,
						HorizontalOptions = LayoutOptions.Center,//中央に配置する（横方向）
					};
					int tmp_i = i; //avoid delegate error
					entry[i].TextChanged += (sender, e) => {
						string tmp = entry[tmp_i].Text;
						for(int j = 0; j < size; ++j) {
							entry[j].Text = tmp;
						}
					};
					stepper[i].ValueChanged += (sender, e) => {
						int displayTime;
						if(int.TryParse(GetText(), out displayTime)) {
							if(e.OldValue < e.NewValue) ++displayTime;
							else --displayTime;
							SetText(displayTime.ToString());
						}
					};
				}
			}
			public StackLayout GetStackLayout(int i, StackOrientation orientation = StackOrientation.Horizontal) {
				return new StackLayout {
					Orientation = orientation,
					Children = {
						entry[i],
						stepper[i]
					}
				};
			}
			public Entry GetEntry(int i) {
				return entry[i];
			}
			public void SetText(string str) {
				for(int j = 0; j < size; ++j) {
					entry[j].Text = str;
				}
			}
			public string GetText() {
				return entry[0].Text;
			}
		}

		class HsChange {
			public int n;
			public double delta;
			public double hsStart;
			public double hsAdjust;
			public double hsBpm;

			public ViewCellHsChange ToViewCell() {
				if(hsStart > 10 || hsAdjust > 10 || hsBpm > MAX_HSBPM) {
					return new ViewCellHsChange();
				}
				else {
					var cell = new ViewCellHsChange();
					int deltaN = (int)(delta * 2 + 0.05);
					if(hsStart > hsAdjust) deltaN = -deltaN;
					char tmpchar = ' ';
					string tmpstr = "";
					if(deltaN == 0) {
						tmpstr = "";
						tmpchar = ' ';
					}
					else if(deltaN > 0) {
						tmpstr = string.Format(" (黒鍵{0}回)", deltaN);
						tmpchar = '+';
					}
					else {
						tmpstr = string.Format(" (白鍵{0}回)", -deltaN);
						tmpchar = '-';
					}
					cell.delta = string.Format("{0}{1,4:0.00}{2}", tmpchar, delta, tmpstr);
					cell.hs = string.Format("HS {0,4:0.00} → {1,4:0.00}", hsStart, hsAdjust);
					cell.hsBpm = string.Format("{0}", (int)hsBpm);
					return cell;
				}
			}
		}
		class ViewCellHsChange : ViewCell {
			public static readonly BindableProperty propertyHs =
				BindableProperty.Create("hs", typeof(string), typeof(ViewCellHsChange), "");
			public string hs {
				get { return (string)GetValue(propertyHs); }
				set { SetValue(propertyHs, value); }
			}
			public static readonly BindableProperty propertyHsBpm =
				BindableProperty.Create("hsBpm", typeof(string), typeof(ViewCellHsChange), "");
			public string hsBpm {
				get { return (string)GetValue(propertyHsBpm); }
				set { SetValue(propertyHsBpm, value); }
			}
			public static readonly BindableProperty propertyDelta =
				BindableProperty.Create("delta", typeof(string), typeof(ViewCellHsChange), "");
			public string delta {
				get { return (string)GetValue(propertyDelta); }
				set { SetValue(propertyDelta, value); }
			}
		}

		class NhsChange {
			public int n;
			public double delta;
			public int deltaN;
			public double hsStart;
			public double hsAdjust;
			public double hsBpmStart;
			public double hsBpmAdjust;
			public double DisplayTimeStart;
			public double DisplayTimeAdjust;

			public ViewCellNhsChange ToViewCell() {
				if(hsStart > 4.0 || hsAdjust > 4.0 || hsBpmStart > MAX_HSBPM || hsBpmAdjust > MAX_HSBPM) {
					return new ViewCellNhsChange();
				}
				else {
					var cell = new ViewCellNhsChange();

					string tmpstr;
					char tmpchar;
					if(deltaN == 0) {
						tmpstr = "";
						tmpchar = ' ';
					}
					else if(deltaN > 0) {
						tmpstr = string.Format(" (黒鍵{0}回)", deltaN);
						tmpchar = '+';
					}
					else {
						tmpstr = string.Format(" (白鍵{0}回)", -deltaN);
						tmpchar = '-';
					}
					cell.delta = string.Format("{0}{1,3:0.00}{2}", tmpchar, Math.Abs(delta), tmpstr);
					cell.hs = string.Format("HS {0,3:0.00} → {1,3:0.00}", hsStart, hsAdjust);
					cell.hsBpm = string.Format("{0} → {1}", (int)hsBpmStart, (int)hsBpmAdjust);
					cell.displayTime = string.Format("{0} → {1}", (int)DisplayTimeStart, (int)DisplayTimeAdjust);
					return cell;
				}
			}
		}
		class ViewCellNhsChange : ViewCell {
			public static readonly BindableProperty propertyHs =
				BindableProperty.Create("hs", typeof(string), typeof(ViewCellNhsChange), "");
			public string hs {
				get { return (string)GetValue(propertyHs); }
				set { SetValue(propertyHs, value); }
			}
			public static readonly BindableProperty propertyHsBpm =
				BindableProperty.Create("hsBpm", typeof(string), typeof(ViewCellNhsChange), "");
			public string hsBpm {
				get { return (string)GetValue(propertyHsBpm); }
				set { SetValue(propertyHsBpm, value); }
			}
			public static readonly BindableProperty propertyDisplayTime =
				BindableProperty.Create("displayTime", typeof(string), typeof(ViewCellNhsChange), "");
			public string displayTime {
				get { return (string)GetValue(propertyDisplayTime); }
				set { SetValue(propertyDisplayTime, value); }
			}

			public static readonly BindableProperty propertyDelta =
				BindableProperty.Create("delta", typeof(string), typeof(ViewCellNhsChange), "");
			public string delta {
				get { return (string)GetValue(propertyDelta); }
				set { SetValue(propertyDelta, value); }
			}
		}

		double NhsTable(int n) {
			switch(n) {
				case 1:
					return 1.0;
				case 2:
					return 1.5;
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
					return (0.25 * (n - 3)) + 2.0;
				default:
					throw new ArgumentException("argument 'n' must be 1-11", nameof(n));
			}
		}

		int FhsToNhsN(double Fhs) {
			if(Fhs < 2.88) {
				if(Fhs < 1.76) {
					if(Fhs < 1.26) return 1;
					else return 2;
				}
				else if(Fhs < 2.38) {
					if(Fhs < 2.13) return 3;
					else return 4;
				}
				else {
					if(Fhs < 2.63) return 5;
					else return 6;
				}
			}
			else {
				if(Fhs < 3.38) {
					if(Fhs < 3.13) return 7;
					else return 8;
				}
				else if(Fhs < 3.88) {
					if(Fhs < 3.63) return 9;
					else return 10;
				}
				else return 11;
			}
		}
		double FhsToNhs(double Fhs) {
			return NhsTable(FhsToNhsN(Fhs));
		}

		class ViewCellFhsToNhs : ViewCell {
			public static readonly BindableProperty propertyHs =
				BindableProperty.Create("hs", typeof(string), typeof(ViewCellNhsChange), "");
			public string hs {
				get { return (string)GetValue(propertyHs); }
				set { SetValue(propertyHs, value); }
			}
			public static readonly BindableProperty propertyHsBpm =
				BindableProperty.Create("hsBpm", typeof(string), typeof(ViewCellNhsChange), "");
			public string hsBpm {
				get { return (string)GetValue(propertyHsBpm); }
				set { SetValue(propertyHsBpm, value); }
			}
		}
		ViewCellFhsToNhs[] arrayViewCellFhsToNhs;
		ListView listViewFhsToNhs;

		class MusicListInfo{
			public int Version { get; set; }
			public string UpdatedAt { get; set; }
			public string MinimumAppVersion { get; set; }

			public DateTime DateTime {
				get {
					return DateTime.Parse(UpdatedAt);
				}
			}
		}

		MusicListInfo info;

		class ViewCellMusic : ViewCell {
			public static readonly BindableProperty propertyTitle =
				BindableProperty.Create("title", typeof(string), typeof(ViewCellMusic), "");
			public string title {
				get { return (string)GetValue(propertyTitle); }
				set { SetValue(propertyTitle, value); }
			}
			public static readonly BindableProperty propertySubtitle =
				BindableProperty.Create("subtitle", typeof(string), typeof(ViewCellMusic), "");
			public string subtitle {
				get { return (string)GetValue(propertySubtitle); }
				set { SetValue(propertySubtitle, value); }
			}
			public static readonly BindableProperty propertyBpm =
				BindableProperty.Create("bpm", typeof(string), typeof(ViewCellMusic), "");
			public string bpm {
				get { return (string)GetValue(propertyBpm); }
				set { SetValue(propertyBpm, value); }
			}
			public string bpmStart { get; set; }
			public string bpmAdjust { get; set; }
			public string id { get; set; }
			public int version { get; set; }
			public string noScore { get; set; }
		}

		class ScorePage : SafeAreaPage {
			private ScorePage() { }
			public ScorePage(string title, string url) {
				Title = title;
				Content = new WebView {
					Source = url,
					VerticalOptions = LayoutOptions.FillAndExpand
				};
			}
		}

		class MusicPage : SafeAreaPage {
			#region score_definition
			const int N = 0;
			const int H = 1;
			const int A = 2;
			const int MAX_SCORE = 3;
			string ScoreToStr(int score) {
				switch(score) {
					case N:
						return "N";
					case H:
						return "H";
					case A:
						return "A";
				}
				throw new ArgumentException();
			}
			#endregion

			#region playstyle_definition
			const int SP1 = 0;
			const int SP2 = 1;
			const int DP = 2;
			const int MAX_PLAY_STYLE = 3;

			string PlayStyleToStr(int style) {
				switch(style) {
					case SP1:
						return "SP";
					case SP2:
						return "SP";
					case DP:
						return "DP";
				}
				throw new ArgumentException();
			}
			char PlayStyleToId(int style) {
				switch(style) {
					case SP1:
						return '1';
					case SP2:
						return '2';
					case DP:
						return 'D';
				}
				throw new ArgumentException();
			}
			string PlayStyleScoreToStr(int style, int score) {
				return PlayStyleToStr(style) + ScoreToStr(score);
			}
			#endregion

			static public Dictionary<string, Editor> dicMusicMemo = new Dictionary<string, Editor>();

			private MusicPage() { }
			public MusicPage(ViewCellMusic music) {
				var buttons = new Button[MAX_SCORE, MAX_PLAY_STYLE];
				var grid = new Grid {
					RowSpacing = 0,
					RowDefinitions = {
						new RowDefinition{ Height = new GridLength(8,GridUnitType.Star) },
						new RowDefinition{ Height = new GridLength(10,GridUnitType.Star) },
						new RowDefinition{ Height = new GridLength(10,GridUnitType.Star) },
						new RowDefinition{ Height = new GridLength(10,GridUnitType.Star) },
					},
					ColumnDefinitions = {
						new ColumnDefinition{ Width = new GridLength(1,GridUnitType.Star) },
						new ColumnDefinition{ Width = new GridLength(1,GridUnitType.Star) },
						new ColumnDefinition{ Width = new GridLength(1,GridUnitType.Star) },
					},
				};

				var labelSP1 = new Label {
					Text = "SP(1P)",
					HorizontalOptions = LayoutOptions.Center
				};
				var labelSP2 = new Label {
					Text = "SP(2P)",
					HorizontalOptions = LayoutOptions.Center
				};
				var labelDP = new Label {
					Text = "DP",
					HorizontalOptions = LayoutOptions.Center
				};
				grid.Children.Add(labelSP1, 0, 0);
				grid.Children.Add(labelSP2, 1, 0);
				grid.Children.Add(labelDP, 2, 0);

				for(int style = 0; style < MAX_PLAY_STYLE; ++style) {
					for(int score = 0; score < MAX_SCORE; ++score) {
						string playstylescore = PlayStyleScoreToStr(style, score);
						buttons[style, score] = new Button {
							HorizontalOptions = LayoutOptions.Center,
							Text = playstylescore,
						};
						if(music.noScore.Contains(playstylescore)) {
							buttons[style, score].TextColor = Color.Gray;
						}
						else {
							buttons[style, score].TextColor = Color.Blue;
							int tmp_style = style, tmp_score = score;
							buttons[style, score].Clicked += (sender, e) => {
								string url = string.Format(
									"http://textage.cc/score/{0}.html?{1}{2}",
									music.id,
									PlayStyleToId(tmp_style),
									ScoreToStr(tmp_score)
								);
								Device.OpenUri(new Uri(url));
							};
						}
						grid.Children.Add(buttons[style, score], style, score + 1);
					}
				}
				Content = new StackLayout {
					Children = {
						new StackLayout{
							Spacing = 0,
							VerticalOptions = LayoutOptions.FillAndExpand,
							Children = {
								new StackLayout{
									Padding = new Thickness( 10,PADDING_TOP,0,0),
									VerticalOptions = LayoutOptions.FillAndExpand,
									Children = {
										new Label{ Text = "   メモ" },
										new BoxView{
											Color = Color.Gray,// 塗りつぶし色
								            WidthRequest = 320,// 横のサイズ
								            HeightRequest = 1,// 縦のサイズ
										},
										dicMusicMemo[music.id],
										new BoxView{
											Color = Color.Gray,// 塗りつぶし色
								            WidthRequest = 320,// 横のサイズ
								            HeightRequest = 1,// 縦のサイズ
										},
										new Label{ Text = "   譜面", HeightRequest = 30 },
									}
								},
								grid
							}

						},
						adbanners[PAGE_MUSIC_DETAIL],
					}
				};
				Title = music.title;
			}
		}
		#endregion

		public App() {
			#region page_displaytime

			foreach(Entry entry in entryDisplayTime.entry) {
				entry.Keyboard = Keyboard.Numeric;
				entry.HorizontalOptions = LayoutOptions.FillAndExpand;
			}
			foreach(Entry entry in entryBpmStart.entry) {
				entry.Keyboard = Keyboard.Numeric;
				entry.HorizontalOptions = LayoutOptions.FillAndExpand;
			}
			foreach(Entry entry in entryBpmAdjust.entry) {
				entry.Keyboard = Keyboard.Numeric;
				entry.HorizontalOptions = LayoutOptions.FillAndExpand;
			}

			for(int i = 0; i < MAX_PAGE; ++i) {
				adbanners[i] = new AdMobBanner {
					WidthRequest = 320,
					HeightRequest = 50,
					MinimumHeightRequest = 50,
				};
			};

			var labelDisplayTimeStart = new Label {
				Text = "0",
				TextColor = Color.Lime,
				FontSize = 30,
				HorizontalTextAlignment = TextAlignment.Center,
				HorizontalOptions = LayoutOptions.Center
			};

			var buttonUsage = new Button {
				Text = "  使い方",
				FontSize = 15,
				HorizontalOptions = LayoutOptions.Start
			};
			var buttonCalc = new Button {
				Text = "計算",
				TextColor = Color.Red,
				FontSize = 30,
			};
			var buttonCalc2 = new Button {
				Text = "計算",
				TextColor = Color.Red,
				FontSize = 30,
			};
			var buttonCalc3 = new Button {
				Text = "計算",
				TextColor = Color.Red,
				FontSize = 30,
			};
			var buttonCalc4 = new Button {
				Text = "計算",
				TextColor = Color.Red,
				FontSize = 30,
			};

			var height_multi = 0.8;
			var spacing = 0.8;
			if(Device.RuntimePlatform == Device.iOS) {
				height_multi = 1;
				spacing = 5;
			}
			var pageDisplayTimeCalc = new SafeAreaPage {
				Title = "緑数字の計算",
				//Icon = "DisplayTimeCalc.png",
				Content = new StackLayout {
					Padding = new Thickness(0, PADDING_TOP, 0, 0),
					Spacing = 1,
					Children = {
						buttonUsage,
						new StackLayout
						{
							Padding = new Thickness(10, 0, 10, 10),
							VerticalOptions = LayoutOptions.CenterAndExpand,
							Spacing = spacing,
							Children = {
								new Label { Text = "   ノーツ表示時間(緑数字)" },
								entryDisplayTime.GetStackLayout(PAGE_DISPLAY_TIME_CALC, StackOrientation.Horizontal),
								new Label { Text = "   開始時のBPM" },
								entryBpmStart.GetStackLayout(PAGE_DISPLAY_TIME_CALC),
								new Label { Text = "   合わせたいBPM" },
								entryBpmAdjust.GetStackLayout(PAGE_DISPLAY_TIME_CALC),
								new Label { HeightRequest = 30 * height_multi },
								new Label { Text = "   開始時のノーツ表示時間(緑数字)" },
								new Label { HeightRequest = 20 * height_multi },
								labelDisplayTimeStart,
								new Label { HeightRequest = 30 * height_multi },
								buttonCalc,
							}
						},
						adbanners[PAGE_DISPLAY_TIME_CALC],
					}
				}
			};
			#endregion

			#region page_fhscalc
			arrayHsChange = new HsChange[MAX_HS_CHANGE_TIME + 1];
			arrayViewCellHsChange = new ViewCellHsChange[MAX_HS_CHANGE_TIME + 1];
			for(int i = 0; i <= MAX_HS_CHANGE_TIME; ++i) {
				arrayHsChange[i] = new HsChange();
				arrayViewCellHsChange[i] = new ViewCellHsChange();
				arrayViewCellHsChange[i].hs = ""; //i.ToString();
				arrayViewCellHsChange[i].hsBpm = "";
				arrayViewCellHsChange[i].delta = "";
			}

			listViewHsChange = new ListView {
				RowHeight = 60,
				ItemsSource = arrayViewCellHsChange,
				ItemTemplate = new DataTemplate(() => {
					var cell = new ViewCellHsChange();

					var labelHs = new Label {
						FontSize = 20,
						HorizontalTextAlignment = TextAlignment.Center
					};
					labelHs.SetBinding(Label.TextProperty, "hs");
					var labelHsBpm = new Label {
						FontSize = 20,
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalOptions = LayoutOptions.Center,
					};
					labelHsBpm.SetBinding(Label.TextProperty, "hsBpm");
					var labelDelta = new Label {
						FontSize = 15,
						TextColor = Color.Gray,
						HorizontalTextAlignment = TextAlignment.Center
					};
					labelDelta.SetBinding(Label.TextProperty, "delta");
					var gridViewHsChange = new Grid {
						HorizontalOptions = LayoutOptions.FillAndExpand,
						VerticalOptions = LayoutOptions.Center,
						RowDefinitions =
						{
							new RowDefinition() { Height = GridLength.Auto }
						},
						ColumnDefinitions =
						{
							new ColumnDefinition() { Width = new GridLength(18, GridUnitType.Star) },
							new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) },
							new ColumnDefinition() { Width = new GridLength(10, GridUnitType.Star) },
							new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Absolute) },
						},
					};
					var stackHsDelta = new StackLayout {
						HorizontalOptions = LayoutOptions.Center,
						Children = {
							new Label{ HeightRequest = 2 },
							labelHs,
							labelDelta,
						}
					};
					var box = new BoxView {
						Color = Color.Gray,// 塗りつぶし色
						WidthRequest = 1,// 横のサイズ
						HeightRequest = 60,// 縦のサイズ
					};
					gridViewHsChange.Children.Add(stackHsDelta, 0, 0);
					gridViewHsChange.Children.Add(box, 1, 0);
					gridViewHsChange.Children.Add(labelHsBpm, 2, 0);

					cell.View = gridViewHsChange;

					return cell;
				}),
			};

			var gridFhsCalc = new Grid {
				Padding = new Thickness(10, 5, 10, 10),
				RowDefinitions = {
					new RowDefinition{ Height = new GridLength(1,GridUnitType.Auto) },
					new RowDefinition{ Height = new GridLength(1,GridUnitType.Auto) },
					new RowDefinition{ Height = new GridLength(1,GridUnitType.Auto) },
					new RowDefinition{ Height = new GridLength(1,GridUnitType.Auto) },
				},
				ColumnDefinitions = {
					new ColumnDefinition{ Width = new GridLength(1,GridUnitType.Star) },
				},
			};
			gridFhsCalc.Children.Add(new Label { Text = "   開始時のBPM" }, 0, 0);
			gridFhsCalc.Children.Add(entryBpmStart.GetStackLayout(PAGE_FHS_CALC), 0, 1);
			gridFhsCalc.Children.Add(new Label { Text = "   合わせたいBPM" }, 0, 2);
			gridFhsCalc.Children.Add(entryBpmAdjust.GetStackLayout(PAGE_FHS_CALC), 0, 3);

			var pageHsCalc = new SafeAreaPage {
				Title = "FHSの計算",
				//Icon = "FhsCalc.png",
				Content = new StackLayout {
					Spacing = 0.0,
					Padding = new Thickness(0, PADDING_TOP, 0, 0),
					Children = {
						gridFhsCalc,
						new BoxView{
							Color = Color.Gray,
							HorizontalOptions = LayoutOptions.FillAndExpand,
							HeightRequest = 1,
							WidthRequest = 310
						},
						listViewHsChange,
						new BoxView{
							Color = Color.Gray,
							HorizontalOptions = LayoutOptions.FillAndExpand,
							HeightRequest = 1,
							WidthRequest = 310
						},
						buttonCalc2,
						adbanners[PAGE_FHS_CALC],
					},
				},
			};
			#endregion

			#region page_nhscalc
			switchAdjustBpmStart = new Switch() {
				HorizontalOptions = LayoutOptions.Center
			};
			arrayNhsChange = new NhsChange[MAX_NHS_N + 1];
			arrayViewCellNhsChange = new ViewCellNhsChange[MAX_NHS_N + 1];
			for(int i = 0; i <= MAX_NHS_N; ++i) {
				arrayNhsChange[i] = new NhsChange();
				arrayViewCellNhsChange[i] = new ViewCellNhsChange();
				arrayViewCellNhsChange[i].hs = ""; //i.ToString();
				arrayViewCellNhsChange[i].hsBpm = "";
				arrayViewCellNhsChange[i].delta = "";
			}
			listViewNhsChange = new ListView {
				RowHeight = 60,
				ItemsSource = arrayViewCellNhsChange,
				ItemTemplate = new DataTemplate(() => {
					var cell = new ViewCellNhsChange();

					var labelHs = new Label {
						FontSize = 20,
						HorizontalTextAlignment = TextAlignment.Center
					};
					labelHs.SetBinding(Label.TextProperty, "hs");
					var labelHsBpm = new Label {
						FontSize = 17,
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalOptions = LayoutOptions.Center,
					};
					labelHsBpm.SetBinding(Label.TextProperty, "hsBpm");
					var labelDelta = new Label {
						FontSize = 15,
						TextColor = Color.Gray,
						HorizontalTextAlignment = TextAlignment.Center
					};
					labelDelta.SetBinding(Label.TextProperty, "delta");
					var labelDisplayTime = new Label {
						FontSize = 17,
						TextColor = Color.Lime,
						HorizontalTextAlignment = TextAlignment.Center
					};
					labelDisplayTime.SetBinding(Label.TextProperty, "displayTime");
					var grid = new Grid {
						RowDefinitions = {
							new RowDefinition{ Height = new GridLength(1,GridUnitType.Auto) },
						},
						ColumnDefinitions = {
							new ColumnDefinition() { Width = new GridLength(18, GridUnitType.Star) },
							new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) },
							new ColumnDefinition() { Width = new GridLength(10, GridUnitType.Star) },
							new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Absolute) },
						},
					};
					var stack1 = new StackLayout {
						HorizontalOptions = LayoutOptions.Center,
						Children = {
							new Label{ HeightRequest = 2 },
							labelHs,
							labelDelta,
						}
					};
					var stack2 = new StackLayout {
						HorizontalOptions = LayoutOptions.Center,
						Children = {
							new Label{ HeightRequest = 2 },
							labelDisplayTime,
							labelHsBpm,
						}
					};
					var box = new BoxView {
						Color = Color.Gray,
						HeightRequest = 60,
					};
					grid.Children.Add(stack1, 0, 0);
					grid.Children.Add(box, 1, 0);
					grid.Children.Add(stack2, 2, 0);

					cell.View = grid;

					return cell;
				}),
			};

			var gridNhsCalc = new Grid {
				Padding = new Thickness(10, 5, 10, 10),
				RowDefinitions = {
					new RowDefinition{ Height = new GridLength(1,GridUnitType.Auto) },
					new RowDefinition{ Height = new GridLength(1,GridUnitType.Auto) },
					new RowDefinition{ Height = new GridLength(1,GridUnitType.Auto) },
					new RowDefinition{ Height = new GridLength(1,GridUnitType.Auto) },
					//new RowDefinition{ Height = new GridLength(3,GridUnitType.Absolute) },
				},
				ColumnDefinitions = {
					new ColumnDefinition{ Width = new GridLength(1,GridUnitType.Star) },
					new ColumnDefinition{ Width = new GridLength(1,GridUnitType.Star) },
				},
			};
			gridNhsCalc.Children.Add(new Label { Text = "   緑数字" }, 0, 0);
			gridNhsCalc.Children.Add(new Label { Text = "開始BPMに最適化", HorizontalTextAlignment = TextAlignment.Center }, 1, 0);
			gridNhsCalc.Children.Add(entryDisplayTime.GetEntry(PAGE_NHS_CALC), 0, 1);
			gridNhsCalc.Children.Add(switchAdjustBpmStart, 1, 1);
			gridNhsCalc.Children.Add(new Label { Text = "   開始時のBPM" }, 0, 2);
			gridNhsCalc.Children.Add(new Label { Text = "   合わせたいBPM" }, 1, 2);
			gridNhsCalc.Children.Add(entryBpmStart.GetEntry(PAGE_NHS_CALC), 0, 3);
			gridNhsCalc.Children.Add(entryBpmAdjust.GetEntry(PAGE_NHS_CALC), 1, 3);

			var pageNhsCalc = new SafeAreaPage {
				Title = "NHSの計算",
				//Icon = "NhsCalc.png",
				Content = new StackLayout {
					VerticalOptions = LayoutOptions.Center,
					Spacing = 0.0,
					Padding = new Thickness(0, PADDING_TOP, 0, 0),
					Children = {
						gridNhsCalc,
						new BoxView{
							Color = Color.Gray,
							HorizontalOptions = LayoutOptions.FillAndExpand,
							HeightRequest = 1,
							WidthRequest = 310
						},
						listViewNhsChange,
						new BoxView{
							Color = Color.Gray,
							HorizontalOptions = LayoutOptions.FillAndExpand,
							HeightRequest = 1,
							WidthRequest = 310
						},
						buttonCalc3,
						adbanners[PAGE_NHS_CALC],
					},
				},
			};
			#endregion

			#region page_fhs_to_nhs
			arrayViewCellFhsToNhs = new ViewCellFhsToNhs[MAX_NHS_N + 1];
			for(int i = 0; i < MAX_NHS_N + 1; ++i) {
				arrayViewCellFhsToNhs[i] = new ViewCellFhsToNhs {
					hs = "",
					hsBpm = ""
				};
			}
			listViewFhsToNhs = new ListView {
				RowHeight = 60,
				ItemsSource = arrayViewCellFhsToNhs,
				ItemTemplate = new DataTemplate(() => {
					var cell = new ViewCellFhsToNhs();
					var labelHs = new Label {
						FontSize = 20,
						HorizontalTextAlignment = TextAlignment.Center
					};
					labelHs.SetBinding(Label.TextProperty, "hs");
					var labelHsBpm = new Label {
						FontSize = 20,
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalOptions = LayoutOptions.Center,
					};
					labelHsBpm.SetBinding(Label.TextProperty, "hsBpm");
					var gridViewFhsToNhs = new Grid {
						HorizontalOptions = LayoutOptions.FillAndExpand,
						VerticalOptions = LayoutOptions.Center,
						RowDefinitions =
						{
							new RowDefinition() { Height = GridLength.Auto }
						},
						ColumnDefinitions =
						{
							new ColumnDefinition() { Width = new GridLength(18, GridUnitType.Star) },
							new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) },
							new ColumnDefinition() { Width = new GridLength(10, GridUnitType.Star) },
							new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Absolute) },
						},
					};
					var stackHsDelta = new StackLayout {
						HorizontalOptions = LayoutOptions.Center,
						VerticalOptions = LayoutOptions.Center,
						Children = {
							//new Label{ HeightRequest = 2 },
							labelHs,
						}
					};
					var box = new BoxView {
						Color = Color.Gray,// 塗りつぶし色
						WidthRequest = 1,// 横のサイズ
						HeightRequest = 60,// 縦のサイズ
					};
					gridViewFhsToNhs.Children.Add(stackHsDelta, 0, 0);
					gridViewFhsToNhs.Children.Add(box, 1, 0);
					gridViewFhsToNhs.Children.Add(labelHsBpm, 2, 0);

					cell.View = gridViewFhsToNhs;
					return cell;
				}),
			};
			var gridFhsToNhs = new Grid {
				Padding = new Thickness(10, 5, 10, 10),
				RowDefinitions = {
					new RowDefinition{ Height = new GridLength(1,GridUnitType.Auto) },
					new RowDefinition{ Height = new GridLength(1,GridUnitType.Auto) },
					new RowDefinition{ Height = new GridLength(1,GridUnitType.Auto) },
					new RowDefinition{ Height = new GridLength(1,GridUnitType.Auto) },
				},
				ColumnDefinitions = {
					new ColumnDefinition{ Width = new GridLength(1,GridUnitType.Star) },
					//new ColumnDefinition{ Width = new GridLength(1,GridUnitType.Star) },
				},
			};
			gridFhsToNhs.Children.Add(new Label { Text = "   開始時のBPM" }, 0, 0);
			gridFhsToNhs.Children.Add(entryBpmStart.GetStackLayout(PAGE_FHS_TO_NHS), 0, 1);
			gridFhsToNhs.Children.Add(new Label { Text = "   合わせたいBPM" }, 0, 2);
			gridFhsToNhs.Children.Add(entryBpmAdjust.GetStackLayout(PAGE_FHS_TO_NHS), 0, 3);
			var pageFhsToNhs = new SafeAreaPage {
				Title = "FHS → NHS",
				//Icon = "FhsToNhs.png",
				Content = new StackLayout {
					Spacing = 0.0,
					Padding = new Thickness(0, PADDING_TOP, 0, 0),
					Children = {
						gridFhsToNhs,
						new BoxView{
							Color = Color.Gray,
							HorizontalOptions = LayoutOptions.FillAndExpand,
							HeightRequest = 1,
							WidthRequest = 310
						},
						listViewFhsToNhs,
						new BoxView{
							Color = Color.Gray,
							HorizontalOptions = LayoutOptions.FillAndExpand,
							HeightRequest = 1,
							WidthRequest = 310
						},
						buttonCalc4,
						adbanners[PAGE_FHS_TO_NHS],
					}
				}
			};
			#endregion

			#region page_music
			pageMusic = new SafeAreaPage {
				Title = "曲リスト",
				//Padding = new Thickness(0, PADDING_TOP, 0, 0),
			};
			var updateButton = new ToolbarItem { Name = "更新" };
			pageMusic.ToolbarItems.Add(updateButton);
			var listviewMusic = new ListView {
				RowHeight = 72,
				ItemsSource = listViewCellMusic,
				ItemTemplate = new DataTemplate(() => {
					var cell = new ViewCellMusic();

					var labelStrBpm = new Label {
						FontSize = 16,
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalTextAlignment = TextAlignment.Center,
						Text = "BPM",
					};
					var labelBpm = new Label {
						FontSize = 16,
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalTextAlignment = TextAlignment.Center,
					};
					labelBpm.SetBinding(Label.TextProperty, "bpm");
					var labelTitle = new Label {
						FontSize = 16,
						HorizontalTextAlignment = TextAlignment.Center,
						VerticalOptions = LayoutOptions.Center,
					};
					labelTitle.SetBinding(Label.TextProperty, "title");
					var labelSubtitle = new Label {
						FontSize = 10,
						TextColor = Color.Gray,
						HorizontalTextAlignment = TextAlignment.Center
					};
					labelSubtitle.SetBinding(Label.TextProperty, "subtitle");

					var button = new Button();
					if(Device.RuntimePlatform == Device.iOS) {
						button.Image = "info.png";
						button.TextColor = Color.White;
					}
					else {
						button.Text = ">";
						button.TextColor = Color.Black;
					}
					button.Clicked += async (object sender, EventArgs e) => {
						button.SetBinding(Button.TextProperty, "id");
						string id = button.Text;
						button.RemoveBinding(Button.TextProperty);
						button.Text = "";
						if(Device.RuntimePlatform == Device.Android){
							button.Text = ">";
						}
						var music = listViewCellMusic.Where((obj) => obj.id == id).First();
						if(music != null) {
							await pageMusic.Navigation.PushAsync(new MusicPage(music));
						}
					};
					var box = new BoxView {
						Color = Color.Gray,// 塗りつぶし色
						WidthRequest = 1,// 横のサイズ
						HeightRequest = 72,// 縦のサイズ
					};
					var biggrid = new Grid {
						HorizontalOptions = LayoutOptions.FillAndExpand,
						VerticalOptions = LayoutOptions.Center,
						RowDefinitions =
						{
							new RowDefinition() { Height = GridLength.Auto }
						},
						ColumnDefinitions =
						{
							new ColumnDefinition() { Width = new GridLength(65, GridUnitType.Star) },
							new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Absolute) },
							new ColumnDefinition() { Width = new GridLength(35, GridUnitType.Star) },
							new ColumnDefinition() { Width = new GridLength(30, GridUnitType.Absolute) },
							new ColumnDefinition() { Width = new GridLength(10, GridUnitType.Absolute) },
						}
					};
					var stack = new StackLayout {
						Padding = new Thickness(0, 3, 0, 0),
						HorizontalOptions = LayoutOptions.Center,
						VerticalOptions = LayoutOptions.Center,
						Children =
						{
							labelTitle,
							labelSubtitle,
						}
					};
					var stack2 = new StackLayout {
						Padding = new Thickness(0, 3, 0, 0),
						HorizontalOptions = LayoutOptions.Center,
						VerticalOptions = LayoutOptions.Center,
						Children =
						{
							labelStrBpm,
							labelBpm,
						}
					};
					biggrid.Children.Add(stack, 0, 0);
					biggrid.Children.Add(box, 1, 0);
					biggrid.Children.Add(stack2, 2, 0);
					biggrid.Children.Add(button, 3, 0);

					cell.View = biggrid;

					return cell;
				}),
			};
			pageMusic.Content = new StackLayout {
				HorizontalOptions = LayoutOptions.Center,
				Spacing = 0.0,
				Children = {
					listviewMusic,
					adbanners[PAGE_MUSIC_LIST],
				}
			};
			#endregion

			//var pageSetting = new SafeAreaPage {
			//	Title = "設定",
			//	//Icon = "Setting.png",
			//	Padding = new Thickness(0, PADDING_TOP, 0, 0),
			//	Content = new StackLayout {
			//		VerticalOptions = LayoutOptions.Center,
			//		Children = {
			//			//bannerAdmob3,
			//			//tableSetting
			//		}
			//	},
			//};

			#region event_handlers
			entryDisplayTime.entry[PAGE_DISPLAY_TIME_CALC].Completed += (sender, e) => {
				labelDisplayTimeStart.Text = CalcStartDisplayTime();
			};
			entryBpmStart.entry[PAGE_DISPLAY_TIME_CALC].Completed += (sender, e) => {
				labelDisplayTimeStart.Text = CalcStartDisplayTime();
			};
			entryBpmAdjust.entry[PAGE_DISPLAY_TIME_CALC].Completed += (sender, e) => {
				labelDisplayTimeStart.Text = CalcStartDisplayTime();
			};
			buttonUsage.Clicked += (sender, e) => {
				Device.OpenUri(new Uri("https://bitbucket.org/HY_RORRE/iidxhscalc/wiki/Home"));
			};
			buttonCalc.Clicked += (sender, e) => {
				labelDisplayTimeStart.Text = CalcStartDisplayTime();
			};
			entryBpmStart.entry[PAGE_FHS_CALC].Completed += (sender, e) => {
				CalcHsChange();
			};
			entryBpmAdjust.entry[PAGE_FHS_CALC].Completed += (sender, e) => {
				CalcHsChange();
			};
			buttonCalc2.Clicked += (sender, e) => {
				CalcHsChange();
			};
			entryDisplayTime.entry[PAGE_NHS_CALC].Completed += (sender, e) => {
				CalcNhsChange();
			};
			entryBpmStart.entry[PAGE_NHS_CALC].Completed += (sender, e) => {
				CalcNhsChange();
			};
			entryBpmAdjust.entry[PAGE_NHS_CALC].Completed += (sender, e) => {
				CalcNhsChange();
			};
			buttonCalc3.Clicked += (sender, e) => {
				CalcNhsChange();
			};
			entryBpmStart.entry[PAGE_FHS_TO_NHS].Completed += (sender, e) => {
				CalcFhsToNhs();
			};
			entryBpmAdjust.entry[PAGE_FHS_TO_NHS].Completed += (sender, e) => {
				CalcFhsToNhs();
			};
			buttonCalc4.Clicked += (sender, e) => {
				CalcFhsToNhs();
			};
			pageMusic.Appearing += async (sender, e) => {
				if(!is_music_csv_loaded) {
					await LoadCsvMusic();
					is_music_csv_loaded = true;
				}
			};
			updateButton.Clicked += async (sender, e) => {
				await UpdateMusicList();
			};
			listviewMusic.ItemTapped += (object sender, ItemTappedEventArgs e) => {
				var item = e.Item as ViewCellMusic;
				if(item != null) {
					entryBpmStart.SetText(item.bpmStart);
					entryBpmAdjust.SetText(item.bpmAdjust);

					if(item == last_tapped_cell) {
						++tapcount;
						if(50 < tapcount) {
							DeleteAds(adbanners);
						}

					}
					else {
						tapcount = 0;
						last_tapped_cell = item;
					}
				}
			};
			#endregion
			var navigationpageMusic = new NavigationPage(pageMusic) {
				Title = "曲リスト",
				//Icon = "MusicList.png",
			};

			if(Device.RuntimePlatform == Device.iOS){
				pageDisplayTimeCalc.Icon = "DisplayTimeCalc.png";
				pageHsCalc.Icon = "FhsCalc.png";
				pageNhsCalc.Icon = "NhsCalc.png";
				pageFhsToNhs.Icon = "FhsToNhs.png";
				navigationpageMusic.Icon = "MusicList.png";
			}

			#region page_main
			MainPage = new TabbedPage {
				Children = {
					pageDisplayTimeCalc,
					pageHsCalc,
					pageNhsCalc,
					pageFhsToNhs,
					navigationpageMusic
				},
			};
			#endregion

		}

		#region methods
		protected override void OnStart() {
			// Handle when your app starts
			LoadEntry();
		}
		protected override void OnSleep() {
			// Handle when your app sleeps
			SaveEntry();
		}
		protected override void OnResume() {
			// Handle when your app resumes
		}

		void DeleteAds(AdMobBanner[] adbanners) {
			for(int i = 0; i < MAX_PAGE; ++i) {
				adbanners[i].MinimumHeightRequest = 0;
				adbanners[i].HeightRequest = 0;
			}
		}

		double CalcStartDisplayTime(double DisplayTime, double StartBpm, double AdjustBpm) {
			return (DisplayTime * AdjustBpm / StartBpm);
		}
		string CalcStartDisplayTime() {
			try {
				double result = CalcStartDisplayTime(
					double.Parse(entryDisplayTime.entry[0].Text),
					double.Parse(entryBpmStart.entry[0].Text),
					double.Parse(entryBpmAdjust.entry[0].Text)
				);
				return ((int)(result + 0.5)).ToString();
			}
			catch {
				return "値が不正です";
			}

		}

		HsChange CalcHsChange(int n, double StartBpm, double AdjustBpm) {
			var result = new HsChange();
			result.n = n;
			result.delta = n * 0.5;
			result.hsStart = AdjustBpm * result.delta / Math.Abs(StartBpm - AdjustBpm);
			result.hsAdjust = result.hsStart + (StartBpm > AdjustBpm ? result.delta : -result.delta);
			result.hsBpm = result.hsStart * StartBpm;
			return result;
		}
		void CalcHsChange() {
			try {
				double StartBpm = double.Parse(entryBpmStart.entry[0].Text);
				double AdjustBpm = double.Parse(entryBpmAdjust.entry[0].Text);
				arrayViewCellHsChange[0].delta = string.Format("×{0,3:0.00}", AdjustBpm / StartBpm);
				arrayViewCellHsChange[0].hs = string.Format("BPM {0,3} → {1,3}", StartBpm, AdjustBpm);
				arrayViewCellHsChange[0].hsBpm = "HS×BPM";
				for(int i = 1; i <= MAX_HS_CHANGE_TIME; ++i) {
					arrayHsChange[i] = CalcHsChange(i, StartBpm, AdjustBpm);
					arrayViewCellHsChange[i] = arrayHsChange[i].ToViewCell();
				}

				listViewHsChange.ItemsSource = new int[1];
				listViewHsChange.ItemsSource = arrayViewCellHsChange;
			}
			catch {
				listViewHsChange.ItemsSource = new int[MAX_HS_CHANGE_TIME + 1];
			}
		}

		void CalcNhsChange() {
			try {
				double StartBpm = double.Parse(entryBpmStart.entry[0].Text);
				double AdjustBpm = double.Parse(entryBpmAdjust.entry[0].Text);
				double DisplayTime = double.Parse(entryDisplayTime.entry[0].Text);
				arrayViewCellNhsChange[0].delta = string.Format("×{0,3:0.00}", AdjustBpm / StartBpm);
				arrayViewCellNhsChange[0].hs = string.Format("BPM {0,3} → {1,3}", StartBpm, AdjustBpm);
				arrayViewCellNhsChange[0].hsBpm = "HS×BPM";
				arrayViewCellNhsChange[0].displayTime = "緑数字";
				for(int i = 1; i <= MAX_NHS_N; ++i) {
					arrayNhsChange[i].n = i;
					arrayNhsChange[i].hsStart = NhsTable(i);
					arrayNhsChange[i].hsBpmStart = StartBpm * arrayNhsChange[i].hsStart;
					arrayNhsChange[i].DisplayTimeStart = arrayNhsChange[i].DisplayTimeAdjust = DisplayTime;
					double MinDeltaHsBpm = double.MaxValue;
					for(int j = 1; j <= MAX_NHS_N; ++j) {
						double tmpAdjustHs = NhsTable(j);
						double tmpAdjustHsBpm = tmpAdjustHs * AdjustBpm;
						double DeltaHsBpm = Math.Abs(tmpAdjustHsBpm - arrayNhsChange[i].hsBpmStart);
						if(DeltaHsBpm < MinDeltaHsBpm) {
							MinDeltaHsBpm = DeltaHsBpm;
							arrayNhsChange[i].hsAdjust = tmpAdjustHs;
							arrayNhsChange[i].hsBpmAdjust = tmpAdjustHsBpm;
							arrayNhsChange[i].deltaN = j;
						}
						else break;
					}
					//if Adjust Hs to StartBpm
					if(switchAdjustBpmStart.IsToggled) {
						arrayNhsChange[i].DisplayTimeAdjust =
							DisplayTime * arrayNhsChange[i].hsBpmStart / arrayNhsChange[i].hsBpmAdjust;
					}
					//if Adjust Hs to AdjustBpm
					else {
						arrayNhsChange[i].DisplayTimeStart =
							DisplayTime * arrayNhsChange[i].hsBpmAdjust / arrayNhsChange[i].hsBpmStart;
					}
					arrayNhsChange[i].delta = arrayNhsChange[i].hsAdjust - arrayNhsChange[i].hsStart;
					arrayNhsChange[i].deltaN = arrayNhsChange[i].deltaN - i;

					arrayViewCellNhsChange[i] = arrayNhsChange[i].ToViewCell();
				}
				listViewNhsChange.ItemsSource = new int[1];
				listViewNhsChange.ItemsSource = arrayViewCellNhsChange;
			}
			catch {
				listViewNhsChange.ItemsSource = new int[MAX_NHS_N + 1];
			}
		}

		void CalcFhsToNhs() {
			try {
				var bpmStart = double.Parse(entryBpmStart.GetText());
				var bpmAdjust = double.Parse(entryBpmAdjust.GetText());
				arrayViewCellFhsToNhs[0].hs = string.Format("BPM {0,3} → {1,3}", bpmStart, bpmAdjust);
				arrayViewCellFhsToNhs[0].hsBpm = "HS×BPM";
				int index = 1;
				for(int i = 1; i <= MAX_NHS_N; ++i) {
					double Nhs = NhsTable(i);
					double Fhs = Nhs * bpmAdjust / bpmStart;
					arrayViewCellFhsToNhs[i].hs = "";
					arrayViewCellFhsToNhs[i].hsBpm = "";
					int hsBpm = (int)(bpmAdjust * Nhs);
					if(FhsToNhsN(Fhs) == i && Fhs >= 0.5 && Fhs <= 10.0 && hsBpm <= MAX_HSBPM) {
						arrayViewCellFhsToNhs[index].hs = string.Format("HS {0,3:0.00} → {1,3:0.00}", Fhs, Nhs);
						arrayViewCellFhsToNhs[index].hsBpm = string.Format("{0}", hsBpm);
						++index;
					}
				}
			}
			catch {
				listViewFhsToNhs.ItemsSource = new int[MAX_NHS_N + 1];
			}
		}

		void SaveValue(string key, bool value) {
			Application.Current.Properties[key] = value;
		}
		void SaveValue<T>(string key, T value) where T : class {
			Application.Current.Properties[key] = value;
		}
		bool LoadValueBool(string key) {
			if(Application.Current.Properties.ContainsKey(key)) {
				return (bool)Application.Current.Properties[key];
			}
			return false;
		}
		T LoadValue<T>(string key) where T : class {
			if(Application.Current.Properties.ContainsKey(key)) {
				return (T)Application.Current.Properties[key];
			}
			return null;
		}

		int VersionStrToInt(string str){
			var splitted = str.Split('.');
			int versionnum = 0;
			int i = 1;
			int num;
			foreach(var s in splitted.Reverse()){
				if(int.TryParse(s, out num)){
					versionnum += num * i;
				}
				i *= 100;
			}
			return versionnum;
		}
		async Task<bool> UpdateMusicList(){
			try {
				var exist = await ExistUpdateMusicListInfo();
				bool result = true;
				if(!exist) {
					result = await pageMusic.DisplayAlert("更新はありません", "リストに不具合がある場合、アプリをアップデートしてから強制更新してください。", "閉じる", "強制更新");
				}
				bool update = exist || !result;
				if(update) {
					await DownloadCsvMusic();
					var str = string.Format("リストVer. {0}\n更新日:{1}", info.Version, info.DateTime.ToString("yyyy-MM-dd"));
					await pageMusic.DisplayAlert("更新完了", str, "OK");
				}
				return update;
			}
			catch(Exception){
				await pageMusic.DisplayAlert("更新できませんでした", "アプリをアップデートするか、しばらく待って再試行してください。", "OK");
				return false;
			}
		}
		async Task<bool> DownloadCsvMusic(){
			var url = new Uri("https://raw.githubusercontent.com/HYRORRE/IIDXHSCalcMusicList/master/music.csv");

			string allstr_new;
			using (var client = new HttpClient()){
				allstr_new = await client.GetStringAsync(url);
			}

			var folder = FileSystem.Current.LocalStorage;
			var file = await folder.CreateFileAsync("music.csv", CreationCollisionOption.OpenIfExists);
			if(await LoadCsvMusic(allstr_new)){
				await file.WriteAllTextAsync(allstr_new);
				return true;
			}
			throw new Exception("内部エラー");
		}
		async Task<bool> ExistUpdateMusicListInfo() {
			bool exist_update = false;
			var url = new Uri("https://raw.githubusercontent.com/HYRORRE/IIDXHSCalcMusicList/master/MusicListInfo.json");

			string allstr_new;
			using(var client = new HttpClient()) {
				allstr_new = await client.GetStringAsync(url);
			}

			var info_new = JsonConvert.DeserializeObject<MusicListInfo>(allstr_new);
			info = info_new;
			int minimum = VersionStrToInt(info_new.MinimumAppVersion);
			int current = VersionStrToInt(DependencyService.Get<IAssemblyService>().GetVersionName());
			if(current < minimum) {
				throw new NotSupportedException("アプリの更新が必要です。");
			}

			var folder = FileSystem.Current.LocalStorage;
			var exist = await folder.CheckExistsAsync("MusicListInfo.json");
			IFile file;
			if(exist == ExistenceCheckResult.NotFound) {
				file = await folder.CreateFileAsync("MusicListInfo.json", CreationCollisionOption.FailIfExists);
				exist_update = true;
			}
			else {
				file = await folder.GetFileAsync("MusicListInfo.json");
				try {
					var allstr_old = await file.ReadAllTextAsync();
					var info_old = JsonConvert.DeserializeObject<MusicListInfo>(allstr_old);
					if(info_old.Version < info_new.Version) {
						exist_update = true;
					}
				}
				catch(Exception) {
					exist_update = true;
				}
			}
			if(exist_update) {
				await file.WriteAllTextAsync(allstr_new);
				return true;
			}
			return false;
		}

		/// <summary>
		/// Loads the csv musics.
		/// </summary>
		/// <returns>seccess : true</returns>
		/// <param name="allstr">Allstr.</param>
		async Task<bool> LoadCsvMusic(string allstr = null) {
			SaveEntry();
			listViewCellMusic.Clear();
			if(allstr == null) {
				var folder = FileSystem.Current.LocalStorage;
				var exist = await folder.CheckExistsAsync("music.csv");
				if(exist == ExistenceCheckResult.FileExists) {
					var file = await folder.GetFileAsync("music.csv");
					allstr = await file.ReadAllTextAsync();
				}
				else {
					return false;
				}
			}
			CsvParser parser = new CsvParser(new StringReader(allstr));
			parser.Configuration.HasHeaderRecord = true;  // ヘッダ行はあり

			CsvReader csv = new CsvReader(parser);
			while(csv.Read()) {
				var music = new ViewCellMusic {
					title = csv.GetField("title"),
					subtitle = csv.GetField("subtitle"),
					bpmStart = csv.GetField("bpmStart"),
					bpmAdjust = csv.GetField("bpmAdjust"),
					id = csv.GetField("id"),
					version = int.Parse(csv.GetField("version")),
					noScore = csv.GetField("noScore"),
				};
				if(music.bpmStart.Length != 0) {
					music.bpm = string.Format("{0} → {1}", music.bpmStart, music.bpmAdjust);
				}
				listViewCellMusic.Add(music);
			}
			LoadMusicEntry();
			return true;
		}

		void SaveEntry() {
			SaveValue("entryDisplayTime", entryDisplayTime.entry[0].Text);
			SaveValue("entryBpmStart", entryBpmStart.entry[0].Text);
			SaveValue("entryBpmAdjust", entryBpmAdjust.entry[0].Text);
			SaveValue("switchAdjustBpmStart", switchAdjustBpmStart.IsToggled);
			foreach(var pair in MusicPage.dicMusicMemo) {
				SaveValue(pair.Key, pair.Value.Text);
			}
		}
		void LoadEntry() {
			entryDisplayTime.entry[0].Text = LoadValue<string>("entryDisplayTime");
			entryBpmStart.entry[0].Text = LoadValue<string>("entryBpmStart");
			entryBpmAdjust.entry[0].Text = LoadValue<string>("entryBpmAdjust");
			switchAdjustBpmStart.IsToggled = LoadValueBool("switchAdjustBpmStart");
		}
		void LoadMusicEntry() {
			foreach(var music in listViewCellMusic) {
				var tmp = LoadValue<string>(music.id);
				if(tmp == null) tmp = "";
				MusicPage.dicMusicMemo[music.id] = new Editor {
					Text = tmp,
					VerticalOptions = LayoutOptions.FillAndExpand
				};
			}
		}


		#endregion

	}
}
