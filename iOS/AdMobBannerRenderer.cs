﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Google.MobileAds;
using UIKit;
using CoreGraphics;
using HSCalc;
using HSCalc.iOS;

[assembly: ExportRenderer(typeof(AdMobBanner), typeof(AdMobBannerRenderer))]
namespace HSCalc.iOS
{
	public class AdMobBannerRenderer : ViewRenderer
	{
		const string adUnitID = "ca-app-pub-1154104535471881/9299710450";

		BannerView adMobBanner;
		bool viewOnScreen;

		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged(e);

			if (e.NewElement == null)
				return;

			if (e.OldElement == null)
			{
				UIWindow window = UIApplication.SharedApplication.Windows[0];
				foreach(var w in UIApplication.SharedApplication.Windows)
				{
					if (w.RootViewController != null)
					{
						window = w;
						break;
					}
				}
				adMobBanner = new BannerView(AdSizeCons.Banner, new CGPoint(0, 0))
				{
					AdUnitID = adUnitID,
					RootViewController = window.RootViewController
				};
				adMobBanner.AdReceived += (sender, args) =>
				{
					if (!viewOnScreen) AddSubview(adMobBanner);
					viewOnScreen = true;
				};
				var request = Request.GetDefaultRequest();
				request.Gender = Gender.Male;
				request.TestDevices = new String[]{
					"Simulator",
					"4f6a7777fc81bb492b5f2e799ae8d8ff",
					"cf1d42dc0640e2fd0ed3bee06c56bf9f"
				};
				adMobBanner.LoadRequest(request);
				SetNativeControl(adMobBanner);
			}
		}
	}
}
