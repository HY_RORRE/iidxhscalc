﻿using System;
using Google.MobileAds;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using HSCalc.iOS;
using HSCalc;
using UIKit;
using CoreGraphics;

[assembly: ExportRenderer(typeof(Admob), typeof(AdmobRenderer))]
namespace HSCalc.iOS
{
	public class AdmobRenderer : ViewRenderer
	{
		const string bannerId = "ca-app-pub-3940256099942544/2934735716"; //test ad

		BannerView adView;
		bool viewOnScreen = false;

		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged(e);
			if (Control == null) AddBanner();
		}
		public void AddBanner()
		{
			// Setup your BannerView, review AdSizeCons class for more Ad sizes. 
			adView = new BannerView(size: AdSizeCons.Banner, origin: new CGPoint(0, 0))
			{
				AdUnitID = bannerId,
				RootViewController = UIApplication.SharedApplication.Windows[0].RootViewController,
			};

			// Wire AdReceived event to know when the Ad is ready to be displayed
			adView.AdReceived += (object sender, EventArgs e) =>
			{
				if (!viewOnScreen) this.AddSubview(adView);
				viewOnScreen = true;
			};
			var request = Request.GetDefaultRequest();
			request.Gender = Gender.Male;

			adView.LoadRequest(request);
			base.SetNativeControl(adView);
		}
	}

}
