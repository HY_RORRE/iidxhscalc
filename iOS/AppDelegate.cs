﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

using Google.MobileAds;

namespace HSCalc.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init();

			Firebase.Core.App.Configure();
			MobileAds.Configure("ca-app-pub-1154104535471881~7822977254");

			LoadApplication(new App());

			return base.FinishedLaunching(app, options);
		}

		[Export("window")]
		public UIWindow GetWindow()
		{
			return UIApplication.SharedApplication.Windows[0];
		}
	}
}
